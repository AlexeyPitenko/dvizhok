(function () {

    'use strict';

    var $ = require('../node_modules/jquery/dist/jquery.min.js')

    var ModelImporterJson = require('./importers/modelimporterjson.js')


    // var folder = './'

    var ModelImporter = {}

    ModelImporter.fromPath = function(fullFilename) {

      var pathArray = fullFilename.split('/');
      if(pathArray.length === 0)
      {
        pathArray = filename.split('\\');
      }
      console.assert(pathArray.length)
      if(pathArray[pathArray.length - 1] === '')
      {
        pathArray.pop();
      }

      var filename = pathArray[pathArray.length - 1];
      var filenameArray = filename.split('.');
      var ext = filenameArray[filenameArray.length - 1].toLowerCase();

      var folder = pathArray.slice(0, -1).join('/');

      // this._loaded = $.Deferred();

      var ajaxLoaded = $.ajax({
        url: folder + '/' + filename,
        dataType: 'text'
      })

      var model = {};

      ajaxLoaded.done((response) => {
        var importer = null;
        switch(ext)
        {
          case 'json':
            importer = new ModelImporterJson(response, folder);
            break;
          case 'fbx':
            console.error("No fbx importer yet.");
            break;
        }

        model.nodes = importer.nodes;
        // importer.def.done(() => {
        //   this._loaded.resolve();
        // })
      })
      return model;
    }
      // get loaded()
      // {
      //   return this._loaded;
      // }


    module.exports = ModelImporter;

}());
