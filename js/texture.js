(function()
{

  'use strict';

  var $ = require('../node_modules/jquery/dist/jquery.min.js');

  var handleTextureLoaded = function(gl, image, texture)
  {
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.bindTexture(gl.TEXTURE_2D, null);
  }

  class Texture
  {
    constructor(path)
    {
      this.img = new Image();
      this.def = $.Deferred()
      this.isInitGL = false;
      this.isInitGLStarted = false;

      this.img.onload = function()
      {
        this.def.resolve();
      }.bind(this)

      this.img.onerror = function()
      {
        this.def.reject();
      }.bind(this)

      this.img.src = path || './res/defaultTexture.png';
      this.path = path || '';
    }

    initGL(gl)
    {
      if(!this.isInitGLStarted)
      {
        this.isInitGLStarted = true;
        this.textureHandle = gl.createTexture();
        this.def.done(function() {
          handleTextureLoaded(gl, this.img, this.textureHandle)
          this.isInitGL = true;
        }.bind(this))
      }
    }

    get image()
    {
      console.assert(this.ready);
      return this.img;
    }
    get handle()
    {
      console.assert(this.ready);
      return this.textureHandle;
    }
    get ready()
    {
      return this.def.state() === 'resolved' && this.isInitGL;
    }

  }

  module.exports = Texture;

}());
