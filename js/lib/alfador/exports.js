(function () {

    'use strict';

    module.exports = {
        Mat33: require('./Mat33.js'),
        Mat44: require('./Mat44.js'),
        Vec2: require('./Vec2.js'),
        Vec3: require('./Vec3.js'),
        Vec4: require('./Vec4.js'),
        Quaternion: require('./Quaternion.js'),
        Transform: require('./Transform.js'),
        Triangle: require('./Triangle.js')
    };

}());
