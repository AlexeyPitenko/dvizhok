
(function () {

    'use strict';

    var Material = require('./material.js')

    class Node3D
    {
      constructor()
      {

      }

      set material(material)
      {
        this._material = material
      }
      get material()
      {
        return this._material;
      }

      set geometry(geometry)
      {
        this._geometry = geometry;
      }
      get geometry()
      {
        return this._geometry;
      }

    }

    module.exports = Node3D;

}());
