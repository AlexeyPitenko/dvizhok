(function() {
  'use strict';

  var Camera = require('./camera.js')
  var GameObject = require('./gameobject.js')
  var Geometry = require('./geometry.js')
  var Texture = require('./texture.js')
  var data = require('./data.js')
  var $ = require('../node_modules/jquery/dist/jquery.min.js')
  var ShaderPool = require('./shaderPool.js')
  var DeltaTimer = require('./deltatimer.js')
  var Vec4 = require('./lib/alfador/Vec4.js')
  var ModelImporter = require('./modelimporter.js')

  // var KeyCode = {
  //   W: 87,
  //   A: 65,
  //   S: 83,
  //   D: 68
  // }


  class SolaireEngine
  {
    /// Start functions

    constructor()
    {
      this.deltaTimer = new DeltaTimer();
      this.shaderPool = new ShaderPool();
      var canvas = document.getElementById("main-window");
      this.initGL(canvas);
      this.initScene();
      this.pressedKeys = {};

      this.defaultTexture = new Texture();
    }

    initGL(canvas)
    {
      try {
          this.gl = canvas.getContext("webgl2");
          this.gl.viewportWidth = canvas.width;
          this.gl.viewportHeight = canvas.height;


          this.defaultTexture.initGL(this.gl);
      } catch (e) {
        console.log(e)
        if (!this.gl) {
            alert("Could not initialise WebGL, sorry :-(");
            throw e;
        }
      }
    }

    initScene()
    {
      this.objects = [];

      // var gCube = new Geometry(this.gl, data.vertex_cube, data.uv_cube)
      // var tCube = new Texture(this.gl, './res/CompanionCube.png');

      var oCube = new GameObject();
      var cubeModel = ModelImporter.fromPath('./res/cube/cube.json');
      oCube.model = cubeModel;

      var shader = this.shaderPool.loadProgram(this.gl, "simple");
      this.defaultShaderProgram = shader;

      this.objects.push(oCube);
      this.camera = new Camera(this.gl.viewportWidth, this.gl.viewportHeight);
    }

    updateInput()
    {
      var vec = new Vec4(0, 0, 0);
      if(this.pressedKeys['w'])
      {
        // forward
        vec = vec.add(new Vec4(0, 0, -1));
      }
      if(this.pressedKeys['a'])
      {
        // strafe left
        vec = vec.add(new Vec4(1, 0, 0));
      }
      if(this.pressedKeys['s'])
      {
        // backward
        vec = vec.add(new Vec4(0, 0, 1));
      }
      if(this.pressedKeys['d'])
      {
        // strafe right
        vec = vec.add(new Vec4(-1, 0, 0));
      }
      if(this.pressedKeys['r'])
      {
        // up
        vec = vec.add( new Vec4(0, -1, 0));
      }
      if(this.pressedKeys['f'])
      {
        // down
        vec = vec.add(new Vec4(0, 1, 0));
      }

      if(vec.lengthSquared())
      {
        vec = vec.normalize()
      }

      var multiplier = 1;
      if(this.pressedKeys.shift)
      {
        multiplier = 10;
      }

      var newVec = this.camera.getMatrix().multVec4(vec);
      if(newVec.lengthSquared())
      {
        newVec = newVec.normalize();
      }
      var delta = newVec.multScalar(multiplier);
      delta = delta.multScalar(this.deltaTimer.deltaTime);
      this.camera.position = this.camera.position.add(delta);

    }
    bindInput()
    {
      $(document).keydown(function(event) {
        var key = event.key.toLowerCase();
        this.pressedKeys[key] = true

        this.pressedKeys['shift'] = !!window.event.shiftKey

        // event.preventDefault();
        // event.stopPropagation();
        // return false;
      }.bind(this))
      $(document).keyup(function(event) {
        var key = event.key.toLowerCase();
        this.pressedKeys[key] = false

        this.pressedKeys['shift'] = !!window.event.shiftKey
        // event.preventDefault();
        // event.stopPropagation();
        // return false;
      }.bind(this))

      // window.onbeforeunload = function() {
      //   return false;
      // }
    }

    start(FPS)
    {
      this.bindInput();
      setInterval(this.cycle.bind(this), 1000/FPS);
    }

  /// Update functions

    cycle()
    {
      this.deltaTimer.update();
      this.updateInput();
      this.drawScene(this.gl);
    }


    drawScene(gl)
    {
      // gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

      var vpMatrix = this.camera.getMatrix();

      for(var i in this.objects)
      {
        var object = this.objects[i];
        var modelMatrix = object.getMatrix();
        var mvp = modelMatrix.multMat44(vpMatrix);

        // var shaderProgram = object.shaderProgram;
        var shaderProgram = this.defaultShaderProgram;

        var model = object.model;

        for(var j in model.nodes)
        {
          var geometry = model.nodes[j].geometry;
          var material = model.nodes[j].material;
          var texture = material.diffuse;

          geometry.initGL(gl);
          texture.initGL(gl);

          if(geometry.ready && shaderProgram.def.state() === 'resolved')
          {
            // Geometry is loaded
            var buffer = geometry.buffer;
            gl.bindBuffer(gl.ARRAY_BUFFER, buffer);

            this.shaderPool.bindProgram(gl, shaderProgram);

            // gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
            gl.enableVertexAttribArray(shaderProgram.aVertexPosition);
            gl.vertexAttribPointer(shaderProgram.aVertexPosition, 3, gl.FLOAT, false, 5 * 4, 0);

            gl.enableVertexAttribArray(shaderProgram.aTextureCoord);
            gl.vertexAttribPointer(shaderProgram.aTextureCoord, 2, gl.FLOAT, false, 5 * 4, 3 * 4);

            gl.uniformMatrix4fv(shaderProgram.uMVPMatrix, false, mvp.data);

            gl.bindAttribLocation(shaderProgram, shaderProgram.aVertexPosition, "aVertexPosition");
            gl.bindAttribLocation(shaderProgram, shaderProgram.aTextureCoord, "aTextureCoord");
            gl.bindAttribLocation(shaderProgram, shaderProgram.uMVPMatrix, "uMVPMatrix");

            var textureToDraw = this.defaultTexture;
            texture.initGL(this.gl);
            if(texture.ready)
            {
              textureToDraw = texture;
            }

            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, textureToDraw.textureHandle);
            gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);


            gl.drawArrays(gl.TRIANGLES, 0, geometry.length);
          }
        }


      }

      //
      // mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, pMatrix);
      //
      // mat4.identity(mvMatrix);
      //
      // mat4.translate(mvMatrix, [-1.5, 0.0, -7.0]);
      // gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertexPositionBuffer);
      // gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, triangleVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);
      // setMatrixUniforms();
      // gl.drawArrays(gl.TRIANGLES, 0, triangleVertexPositionBuffer.numItems);
    }
  }

  module.exports = SolaireEngine;
}());
