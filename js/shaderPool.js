(function()
{

  'use strict';

  var $ = require('../node_modules/jquery/dist/jquery.min.js');
  class ShaderPool
  {
    constructor()
    {
      this.pool = {};
    }

    // TODO: rewrite
    getShader(gl, id)
    {
      var shaderScript = document.getElementById(id);
      if (!shaderScript)
      {
        return null;
      }

      var str = "";
      var k = shaderScript.firstChild;
      while (k)
      {
        if (k.nodeType == 3)
        {
          str += k.textContent;
        }
        k = k.nextSibling;
      }

      var shader;
      if (shaderScript.type == "x-shader/x-fragment")
      {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
      }
      else if (shaderScript.type == "x-shader/x-vertex")
      {
        shader = gl.createShader(gl.VERTEX_SHADER);
      }
      else
      {
        return null;
      }

      gl.shaderSource(shader, str);
      gl.compileShader(shader);

      if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
      {
        alert(gl.getShaderInfoLog(shader));
        return null;
      }

      return shader;
    }

    loadProgram(gl, name)
    {
      var fragmentShader = this.getShader(gl, "shader-fs");
      var vertexShader = this.getShader(gl, "shader-vs");
      var shaderProgram = gl.createProgram();
      gl.attachShader(shaderProgram, vertexShader);
      gl.attachShader(shaderProgram, fragmentShader);
      gl.linkProgram(shaderProgram);

      if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS))
      {
        alert("Could not initialise shaders");
      }

      gl.useProgram(shaderProgram);

      shaderProgram.aVertexPosition = gl.getAttribLocation(shaderProgram, "aVertexPosition");
      gl.enableVertexAttribArray(shaderProgram.aVertexPosition);
      shaderProgram.aTextureCoord = gl.getAttribLocation(shaderProgram, "aTextureCoord");
      gl.enableVertexAttribArray(shaderProgram.aTextureCoord);

      shaderProgram.uMVPMatrix = gl.getUniformLocation(shaderProgram, "uMVPMatrix");

      shaderProgram.def = $.Deferred().resolve();

      this.pool[name] = shaderProgram;
      return shaderProgram;
    }

    bindProgram(gl, programOrName)
    {
      var shaderProgram = null;
      if(typeof programOrName === 'string')
      {
        shaderProgram = this.pool[name];
      }
      else
      {
        shaderProgram = programOrName;
      }

      gl.useProgram(shaderProgram);
      // shaderProgram.aVertexPosition = gl.getAttribLocation(shaderProgram, "aVertexPosition");
      gl.enableVertexAttribArray(shaderProgram.aVertexPosition);
      // shaderProgram.aTextureCoord = gl.getAttribLocation(shaderProgram, "aTextureCoord");
      gl.enableVertexAttribArray(shaderProgram.aTextureCoord);

      return shaderProgram;
    }

  }

  function setMatrixUniforms()
  {
    gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, mvp);
    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
  }

  module.exports = ShaderPool;

}());
