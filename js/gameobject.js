
(function () {

    'use strict';

    var Vec4 = require('./lib/alfador/Vec4.js');
    var Mat44 = require('./lib/alfador/Mat44.js');
    var Quaternion = require('./lib/alfador/Quaternion.js');

    class GameObject
    {
      constructor()
      {
        this.position = new Vec4(0,0,0);
        this.rotation = new Quaternion(0,0,0)

        this._model = null;
        // this._geometry = null;
        // this._texture = null;
      }

      getMatrix()
      {
        var matrix1 = Mat44.translation(this.position);
        var matrix2 = this.rotation.matrix()
        var modelMatrix = matrix1.multMat44(matrix2);
        return modelMatrix;
      }

      set model(model)
      {
        this._model = model;
      }
      get model()
      {
        return this._model;
      }

    }


    module.exports = GameObject;

}());
