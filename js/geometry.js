(function() {
  'use strict';

  class Geometry
  {
    // TODO: separate CPU data from GPU buffer
    constructor(vertices, indices)
    {
      this.length = vertices.length / 3; // @ number (int)
      console.assert(vertices.length / 3 === indices.length / 2);

      // Pack data
      this.data = [];
      for(var i = 0; i < this.length; i++)
      {
        this.data.push(vertices[i*3 + 0]);
        this.data.push(vertices[i*3 + 1]);
        this.data.push(vertices[i*3 + 2]);
        this.data.push(indices[i*2 + 0]);
        this.data.push(indices[i*2 + 1]);
      }

    }
    initGL(gl)
    {
      if(!this.isInitGL)
      {
        this.isInitGL = true;
        this.buffer = gl.createBuffer(); // @ WebGLBuffer
        gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.data), gl.STATIC_DRAW);
      }
    }
    get ready()
    {
      return this.isInitGL;
    }
  }

  module.exports = Geometry;
}());
