
(function () {
    'use strict';

    var $ = require('../node_modules/jquery/dist/jquery.min.js')
    class DeltaTimer
    {
      constructor()
      {
        this.time = $.now();
      }
      update()
      {
        var time = $.now();
        this.deltaTime = (time - this.time) / 1000;
        this.time = time;
        return this.deltaTime;
      }
      get()
      {
        return this.deltaTime;
      }
    }

    module.exports = DeltaTimer;

}());
