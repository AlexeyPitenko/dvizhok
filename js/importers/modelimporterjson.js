(function () {

    'use strict';

    var IModelImporter = require('./imodelimporter.js')

    // var Geometry = require('./../geometry.js')
    var Texture = require('./../texture.js')
    var Material = require('./../material.js')
    var Geometry = require('./../geometry.js')

    class ModelImporterJson extends IModelImporter
    {
      constructor(fileContent, folder)
      {
        super()
        this.nodes = []
        try {
          var parsed = JSON.parse(fileContent)

          // var imagesDefs = [];

          for(var i in parsed.nodes)
          {
            var parsedNode = parsed.nodes[i];

            var node = {};
            this.nodes.push(node);

            var vertices = parsedNode.vertices;
            var indices = parsedNode.indices;

            node.geometry = new Geometry(vertices, indices)

            var textureUrl;
            if(parsedNode.material.diffuse)
            {
              textureUrl = folder + '/' + parsedNode.material.diffuse;
            }

            node.material = new Material({
              diffuse: new Texture(textureUrl)
            })
          }
        } catch (e) {
          console.log(e)
          throw e;
        } finally {

        }
      }
    }

    module.exports = ModelImporterJson;

}());
