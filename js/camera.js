(function() {
  'use strict';

  var Vec4 = require('./lib/alfador/Vec4.js');
  var Mat44 = require('./lib/alfador/Mat44.js');
  var Quaternion = require('./lib/alfador/Quaternion.js');
  var GameObject = require('./gameobject.js');

  class Camera extends GameObject {
    constructor(viewportWidth, viewportHeight) {
      super();
      this.fov = 90;
      this.viewportWidth = viewportWidth;
      this.viewportHeight = viewportHeight;
    }

    getMatrix() { // override
      var viewMatrix = GameObject.prototype.getMatrix.call(this);
      var projectionMatrix = Mat44.perspective( this.fov, this.viewportWidth / this.viewportHeight, 0.1, 100.0 );
      return projectionMatrix.multMat44(viewMatrix);
    }
  }

  module.exports = Camera;

}());
