
(function () {

    'use strict';

    var Texture = require('./texture.js')

    class Material
    {
      constructor(options)
      {
        this.diffuse = options.diffuse || new Texture();
        // this.shaderProgram = options.shaderProgram || null;
      }

      // set shaderProgram(program)
      // {
      //   this._shaderProgram = program;
      // }
      // get shaderProgram()
      // {
      //   return this._shaderProgram;
      // }
    }

    module.exports = Material;

}());
