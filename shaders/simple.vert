
attribute vec3 aVertexPosition;
attribute vec2 aTextureCoord;

varying highp vec2 vTextureCoord;

uniform mat4 uMVPMatrix;

void main(void) {
  gl_Position = uMVPMatrix * vec4(aVertexPosition, 1.0);
  vTextureCoord = aTextureCoord;
}
